<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required'
        ]);
        $query = DB::table('cast')->insert([
            "Nama" => $request ["nama"],
            "Umur" => $request ["umur"],
            "Bio" => $request ["bio"]
        ]);

        return redirect('/cast')->with('success', 'Cast Berhasil Disimpan!');
    }

    public function index(){
        $casts = DB::table('cast')->get();
        // dd($index);
        return view('cast.index', compact('casts'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        // dd($cast);
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required'
        ]);

        $query=DB::table('cast')
                  ->where('id', $id)
                  ->update([
                      'nama'=>$request['nama'],
                      'umur'=>$request['umur'],
                      'bio'=>$request['bio']
                  ]);
        
        return redirect('/cast')->with('success', 'Cast Berhasil Diupdate!');
    }

    public function destroy($id){
        $query=DB::table('cast')->where('id' , $id)->delete();
        return redirect('/cast')->with('success', 'Cast Berhasil Dihapus!');
    }
}
