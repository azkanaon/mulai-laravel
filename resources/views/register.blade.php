<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome"method="POST">
    @csrf
        <label> First name: </label> <br><br>
        <input type="text" name="namaDepan"> <br><br>
        <label> Last name: </label> <br><br>
        <input type="text" name="namaBelakang"> <br><br>
        <label> Gender: </label> <br><br>
        <input type="radio" name="Gender"  >Male <br>
        <input type="radio" name="Gender"  >Female <br>
        <input type="radio" name="Gender"  >Other <br><br>
        <label> Nationality: </label> <br><br>
        <select name="nationality"> <br>
            <option value="1">Indonesian</option>
            <option value="2">Singaporean</option>
            <option value="3">Malaysian</option>
            <option value="4">Australian</option>
        </select> <br><br>
        <label> Language Spoken: </label> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label> Bio: </label> <br> <br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>