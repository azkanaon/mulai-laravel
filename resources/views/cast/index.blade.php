@extends('adminlte.master')

@section('title')
    Daftar Cast
@endsection
@section('content')
    <div class="ml-3 mt-3">
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" >
                    {{session('success')}}
                </div>
            @endif
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Nama</th>
                  <th style="width: 40px">Umur</th>
                  <th>Bio</th>
                  <th style="width: 200px" >Actions</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($casts as $key => $cast)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $cast->nama }} </td>
                        <td> {{ $cast->umur }} </td>
                        <td> {{ $cast->bio }} </td>
                        <td style="display: flex;">
                            <a href="/cast/{{$cast->id}}" class="btn btn-primary btn-sm">Show</a>
                            <a href="/cast/{{$cast->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                            <form action="/cast/{{$cast->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                                <input type="submit" value="Anai" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="5" align="center">Tidak ada cast</td>
                </tr>
                @endforelse
              </tbody>
            </table>
            <a class="btn btn-primary mt-3"  href="/cast/create">Buat Baru</a>
          </div>
    </div>
@endsection