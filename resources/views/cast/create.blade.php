@extends('adminlte.master')

@section('title')
    Daftar Cast
@endsection

@section('content')
    <!-- form start -->
        <form role="form" action="/cast" method="POST">
        @csrf
            <div class="card-body">
                <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name ="nama" value="{{old('nama','')}}" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" id="umur" name="umur" value="{{old('umur','')}}" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{old('bio','')}}" placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Daftar</button>
            </div>
        </form>   
@endsection